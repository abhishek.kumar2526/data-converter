import { Component } from '@angular/core';
import * as XLSX from 'xlsx';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  totalSum2016 = [];
  totalSum2017 = [];
  totalSum2018 = [];
  xlsData2016: any;
  xlsData2017: any;
  xlsData2018: any;
  xlsMasterObj2016: object = {};
  xlsMasterObj2017: object = {};
  xlsMasterObj2018: object = {};
  characters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O','P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  labelname = {
    'A' : 'PM2',
    'B' : 'CV - Raman - ( PM10 )',
    'C' : 'CV - Raman - ( PM2.5 )',
    'D' : 'CRRI - ( PM10 )',
    'E' : 'CRRI - ( PM2.5 )',
    'F' : 'DU - ( PM10 )',
    'G' : 'DU - ( PM2.5 )',
    'H' : 'IITM - ( PM10 )',
    'I' : 'IITM - ( PM2.5 )',
    'J' : 'IMD - Lr ( PM10 )',
    'K' : 'IMD - Lr ( PM2.5 )',
    'L' : 'Noida ( PM10 )',
    'M' : 'Noida ( PM2.5 )',
    'N' : 'IMD - Ayanagar ( PM10 )',
    'O' : 'IMD - Ayanagar ( PM2.5 )',
    'P' : 'IGI - Airport ( PM10 )',
    'Q' : 'IGI - Airport ( PM2.5 )',
  };

  uploadFile(e) {
    var files = e.target.files, f = files[0];
    var reader = new FileReader();
    reader.onload = (e) => {
    var data = new Uint8Array(e.target['result']);
    var workbook = XLSX.read(data, {type: 'array'});
    
  
      /* DO SOMETHING WITH workbook HERE */
      this.xlsData2016 = workbook['Sheets']['2016'];
      // this.xlsData2017 = workbook['Sheets']['2017'];
      // this.xlsData2018 = workbook['Sheets']['2018'];

      this.characters.forEach(char => {
        this.calcDataByYear(char, this.xlsData2016, this.xlsMasterObj2016);
        // this.calcDataByYear(char, this.xlsData2017, this.xlsMasterObj2017);
        // this.calcDataByYear(char, this.xlsData2018, this.xlsMasterObj2018);
      });
      // console.log(this.xlsData2017);
      this.calculateDailyData(this.xlsMasterObj2016, this.totalSum2016);
      // this.calculateDailyData(this.xlsMasterObj2017, this.totalSum2017);
      // this.calculateDailyData(this.xlsMasterObj2018, this.totalSum2018);
    };
    reader.readAsArrayBuffer(f);

    setTimeout(() => {
      let data2016 = XLSX.utils.json_to_sheet(this.totalSum2016);
      // let data2017 = XLSX.utils.json_to_sheet(this.totalSum2017);
      // let data2018 = XLSX.utils.json_to_sheet(this.totalSum2018);
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, data2016, '2016');
      // XLSX.utils.book_append_sheet(wb, data2017, '2017');
      // XLSX.utils.book_append_sheet(wb, data2018, '2018');
      XLSX.writeFile(wb, 'data_2016.xlsx')
    }, 5000);
  }
  
  
  calcDataByYear(char, data, xlsMasterObj) {
    xlsMasterObj[char] = [];
    for (let key in data) {
      if (data.hasOwnProperty(key)) {
        if ((''+ key).startsWith(char)) {
          let thenum = key.replace( /^\D+/g, ''); 
          if (thenum){
            if (data.hasOwnProperty('A' + thenum)) {
              let obj = {
                date: (data['A' + thenum + '']['w']).split(' ')[0],
                data: data[key]['v']
              }
              xlsMasterObj[char].push(obj);
            }
          }
        }
      }
    }
  }

  calculateDailyData(data, totalSum) {
    console.log(data);
    for (let key in data) {
      // console.log(data[key]);
      if(data[key].length > 0) {
        let date = data[key][1]['date'];
        let count = 0;
        let sum = 0;
        let val = 0;
        if (key !== 'A') {
          data[key].forEach((el, index) => {
              // console.log('=============================');
              let newDate = el['date'];
              // console.log(newDate);
              // console.log(date);
              if (newDate === date) {
                count += Number(el['data']);
                sum++;
              } else {
                let obj = {
                  reading_date: date,
                  daily_total: Number(count).toFixed(2),
                  total_num_of_readings_daily: Number(sum).toFixed(2),
                  daily_average: Number(count/sum).toFixed(2),
                  reading_place: key
                };
                totalSum.push(obj);
                // this.makeMonthlydata(obj);
                count = Number(el['data']);
                sum = 1;
                date = newDate;
              }
              // console.log('=============================');
          });
        }
      }
    }
    // console.log(this.totalSum2018);
    this.rearrangeData(this.totalSum2016);
    // this.rearrangeData(this.totalSum2017);
    // this.rearrangeData(this.totalSum2018);
  }

  rearrangeData(data){
    data.forEach((element, index) => {
      element['reading_place'] = this.labelname[element['reading_place']];
    });

  }

  makeMonthlydata(data) {
    console.log(data);
  }
}
