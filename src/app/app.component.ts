  import { Component } from '@angular/core';
import * as XLSX from 'xlsx';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  totalSum2016 = [];
  totalSum2017 = [];
  totalSum2018 = [];
  xlsData2016: any;
  xlsData2017: any;
  xlsData2018: any;
  xlsMasterObj2016: object = {};
  xlsMasterObj2017: object = {};
  xlsMasterObj2018: object = {};
  year = {
    '01' : 'January',
    '02' : 'February', 
    '03' : 'March',
    '04' : 'April',
    '05' : 'May',
    '06' : 'June',
    '07' : 'July',
    '08' : 'August',
    '09' : 'September',
    '10' : 'October',
    '11' : 'November', 
    '12' : 'December'
  };
  characters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O','P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  labelname = {
    'A' : 'PM2',
    'B' : 'CV - Raman - ( PM10 )',
    'C' : 'CV - Raman - ( PM2.5 )',
    'D' : 'CRRI - ( PM10 )',
    'E' : 'CRRI - ( PM2.5 )',
    'F' : 'DU - ( PM10 )',
    'G' : 'DU - ( PM2.5 )',
    'H' : 'IMD Ayanagar - ( PM10 )',
    'I' : 'IMD Ayanagar - ( PM2.5 )',
    'J' : 'IMD - Lr ( PM10 )',
    'K' : 'IMD - Lr ( PM2.5 )',
    'L' : 'NCMRFW ( PM10 )',
    'M' : 'NCMRFW ( PM2.5 )',
    'N' : 'NISE - Gurugram ( PM10 )',
    'O' : 'NISE - Gurugram - Ayanagar ( PM2.5 )',
    'P' : 'NPL - ( PM10 )',
    'Q' : 'NPL - ( PM2.5 )',
    'R' : 'Palam - IGI Airport ( PM10 )',
    'S' : 'Palam - IGI Airport ( PM2.5 )',
  };

  uploadFile(e) {
    var files = e.target.files, f = files[0];
    var reader = new FileReader();
    reader.onload = (e) => {
    var data = new Uint8Array(e.target['result']);
    var workbook = XLSX.read(data, {type: 'array'});
    
  
      /* DO SOMETHING WITH workbook HERE */
      this.xlsData2016 = workbook['Sheets']['2018'];
      // this.xlsData2017 = workbook['Sheets']['2017'];
      // this.xlsData2018 = workbook['Sheets']['2018'];

      this.characters.forEach(char => {
        this.calcDataByYear(char, this.xlsData2016, this.xlsMasterObj2016);
        // this.calcDataByYear(char, this.xlsData2017, this.xlsMasterObj2017);
        // this.calcDataByYear(char, this.xlsData2018, this.xlsMasterObj2018);
      });
      // console.log(this.xlsData2017);
      this.calculateDailyData(this.xlsMasterObj2016, this.totalSum2016);
      // this.calculateDailyData(this.xlsMasterObj2017, this.totalSum2017);
      // this.calculateDailyData(this.xlsMasterObj2018, this.totalSum2018);
    };
    reader.readAsArrayBuffer(f);

    setTimeout(() => {
      let data2016 = XLSX.utils.json_to_sheet(this.totalSum2016);
      // let data2017 = XLSX.utils.json_to_sheet(this.totalSum2017);
      // let data2018 = XLSX.utils.json_to_sheet(this.totalSum2018);
      let monthly2016 = XLSX.utils.json_to_sheet(this.totalMonthlySum2016);
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, data2016, 'daily');
      // XLSX.utils.book_append_sheet(wb, data2017, '2017');
      // XLSX.utils.book_append_sheet(wb, data2018, '2018');
      XLSX.utils.book_append_sheet(wb, monthly2016, 'monthly');
      XLSX.writeFile(wb, 'total_data_2018.xlsx');
    }, 5000);
  }
  
  
  calcDataByYear(char, data, xlsMasterObj) {
    xlsMasterObj[char] = [];
    for (let key in data) {
      if (data.hasOwnProperty(key)) {
        if ((''+ key).startsWith(char)) {
          let thenum = key.replace( /^\D+/g, ''); 
          if (thenum){
            if (data.hasOwnProperty('A' + thenum)) {
              let obj = {
                date: (data['A' + thenum + '']['w']).split(' ')[0],
                data: data[key]['v']
              }
              xlsMasterObj[char].push(obj);
            }
          }
        }
      }
    }
  }

  calculateDailyData(data, totalSum) {
    for (let key in data) {
      // console.log(data[key]);
      if(data[key].length > 0) {
        let date = data[key][1]['date'];
        let count = 0;
        let sum = 0;
        let val = 0;
        if (key !== 'A') {
          data[key].forEach((el, index) => {
              // console.log('=============================');
              let newDate = el['date'];
              // console.log(newDate);
              // console.log(date);
              if (newDate === date) {
                count += Number(el['data']);
                sum++;
              } else {
                let obj = {
                  reading_date: date,
                  daily_total: Number(count).toFixed(2),
                  total_num_of_readings_daily: Number(sum).toFixed(2),
                  daily_average: Number(count/sum).toFixed(2),
                  reading_place: key
                };
                totalSum.push(obj);
                // this.makeMonthlydata(obj);
                count = Number(el['data']);
                sum = 1;
                date = newDate;
              }
              // console.log('=============================');
          });
        }
      }
    }
    // console.log(this.totalSum2018);
    this.rearrangeData(this.totalSum2016);
    // this.rearrangeData(this.totalSum2017);
    // this.rearrangeData(this.totalSum2018);
    this.makeMonthlydata(data, this.totalMonthlySum2016);
    // this.makeMonthlydata(data, this.totalMonthlySum2017);
    // this.makeMonthlydata(data, this.totalMonthlySum2018);
  }

  rearrangeData(data){
    data.forEach((element, index) => {
      element['reading_place'] = this.labelname[element['reading_place']];
    });
  }

  rearrangeMobthlyData(data){
    // console.log(data);
    data.forEach((element, index) => {
      element['reading_place'] = this.labelname[element['reading_place']];
      element['reading_month'] = this.year[element['reading_month']];
    });
  }

  totalMonthlySum2016 = [];
  totalMonthlySum2017 = [];
  totalMonthlySum2018 = [];
  makeMonthlydata(data, totalMonthlySum) {
    for (let key in data) {
      let sum = 0;
      let count = 0;
      if(data[key].length > 0 && key !== 'A'){
        let oldMonth = data[key][1].date.split('-')[1];
        data[key].forEach((el, index) => {
          console.log(oldMonth);
          if(index > 0) {
            let newMonth = el.date.split('-')[1];
            if (newMonth === oldMonth) {
              sum += el.data;
              count++;
            } else {
              let obj = {
                reading_month: oldMonth,
                monthly_total: Number(sum).toFixed(2),
                total_num_of_readings_calculated_in_month: Number(count).toFixed(2),
                monthly_average: Number(sum/count).toFixed(2),
                reading_place: key
              };
              totalMonthlySum.push(obj);
              count = 0;
              sum = el.data;
              oldMonth = newMonth;
            }
          }
        });
      }
    }
    this.rearrangeMobthlyData(totalMonthlySum);
  }
}
